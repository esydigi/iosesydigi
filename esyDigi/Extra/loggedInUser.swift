//
//  loggedInUser.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 12/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit

class loggedInUser {
    static let shared = loggedInUser()
    
    var string_userID : String
    var string_userName : String
    var string_Email : String
    var string_userDisplayName : String
    
    private init(){
        self.string_userID = ""
        self.string_userName = ""
        self.string_Email = ""
        self.string_userDisplayName = ""
    }
    
    func initializeUserDetails(dicuserdetail : Dictionary<String, Any>) {
        self.string_userID =  String(dicuserdetail["id"] as! NSInteger)
        self.string_userName = dicuserdetail["username"] as! String
        self.string_Email = dicuserdetail["email"] as! String
        self.string_userDisplayName =  dicuserdetail["displayname"] as! String
    }
}
