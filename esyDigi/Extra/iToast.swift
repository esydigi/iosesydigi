//
//  iToast.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 11/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit

class iToast: NSObject {
    
    class func show(_ toastMessage: String?) {
        OperationQueue.main.addOperation({
            let keyWindow: UIWindow? = UIApplication.shared.keyWindow
            let toastView = UILabel()
            //                var attributes: [NSAttributedString.Key : UIFont?]? = nil
            //                if let font = UIFont(name: "HelveticaNeue", size: 15) {
            //                    attributes = [
            //                        NSAttributedString.Key.font: font
            //                    ]
            //                }
            toastView.frame = CGRect(x: 20, y: keyWindow!.bounds.size.height - 50, width: keyWindow!.bounds.size.width - 40, height: 40.0)
            if let font = UIFont(name: "HelveticaNeue", size: 15) {
                toastView.font = font }
            toastView.text = toastMessage
            toastView.textAlignment = NSTextAlignment.center
            toastView.textColor = UIColor.white
            toastView.layer.cornerRadius = 20
            toastView.layer.masksToBounds = true
            toastView.backgroundColor = UIColor.black
            toastView.alpha = 0.8
            keyWindow?.addSubview(toastView)
            UIView.animate(withDuration: 7.0, delay: 0.0, options: .curveEaseOut, animations: {
                toastView.alpha = 0.0
            }) { finished in
                toastView.removeFromSuperview()
            }
        })
    }
}
