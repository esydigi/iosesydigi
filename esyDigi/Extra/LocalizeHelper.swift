//
//  LocalizeHelper.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 22/07/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit
import Foundation



//  The converted code is limited to 1 KB.
//  Please Sign Up (Free!) to double this limit.
//
//  Converted to Swift 5 by Swiftify v5.0.37171 - https://objectivec2swift.com/
// LocalizeHelper.m
// Singleton
//private var SingleLocalSystem: LocalizeHelper? = nil


// my Bundle (not the main bundle!)
private var myBundle: Bundle? = nil

class LocalizeHelper: NSObject {
    static let SingleLocalSystem = LocalizeHelper()
    // Use "LocalizedString(key)" the same way you would use "NSLocalizedString(key,comment)"
    // func LocalizedString(_ key: Any) -> String {
    //    return LocalizeHelper.sharedLocalSystem().localizedString(forKey: (key))
    // }
    
    //    // "language" can be (for american english): "en", "en-US", "english". Analogous for other languages.
    //    func LocalizationSetLanguage(_ language: Any) {
    //        LocalizeHelper.sharedLocalSystem().language = (language)
    //    }
    
    //-------------------------------------------------------------
    // initiating
    //-------------------------------------------------------------
    private override init() {
        super.init()
        // use systems main bundle as default bundle
        myBundle = Bundle.main
    }
    //  The converted code is limited to 1 KB.
    //  Please Sign Up (Free!) to double this limit.
    //
    //  %< ----------------------------------------------------------------------------------------- %<
    //-------------------------------------------------------------
    // translate a string
    //-------------------------------------------------------------
    // you can use this macro:
    // LocalizedString(@"Text");
    
    class func localizedString(forKey key: String?) -> String? {
        // this is almost exactly what is done when calling the macro NSLocalizedString(@"Text",@"comment")
        // the difference is: here we do not use the systems main bundle, but a bundle
        // we selected manually before (see "setLanguage")
        return myBundle?.localizedString(forKey: key ?? "", value: "", table: nil)
    }
    
    //set a new language:
    class  func setLanguage(_ lang: String?) {
        // path to this languages bundle
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        if path == nil {
            // there is no bundle for that language
            // use main bundle instead
            myBundle = Bundle.main
        }
        else {
            // use this bundle as my bundle from now on:
            myBundle = Bundle(path: path ?? "")
            // to be absolutely shure (this is probably unnecessary):
            if myBundle == nil {
                myBundle = Bundle.main
            }
        }
    }
}
