//
//  Constants.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 10/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class Constants: NSObject {
   
    //static let apiURL = "http://testdemo.website:8085/1.0/"
//    static let apiURL = "https://esydigiebokenb.se/api_easydigy/"    //"http://esydigiebokenb.se/api/"
    static let apiURL = "https://esydigiebokenb.com/api_easydigy/"
    static let english = "28255"
    static let swedish = "83"
    static let iap_id = "com.esyDigi.contentbook"

}
