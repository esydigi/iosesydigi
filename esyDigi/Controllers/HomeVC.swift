import Alamofire
import IQKeyboardManagerSwift
import StoreKit

class HomeTableViewCell: UITableViewCell{
    @IBOutlet weak var imageview_Icon: UIImageView!
    @IBOutlet weak var label_Title: UILabel!
}

class HomeVC: UIViewController,UITableViewDataSource,UITableViewDelegate,changeLanguageDelegate{
    @IBOutlet weak var tableView_Home: UITableView!
    @IBOutlet weak var view_fileDownload: UIView!
    @IBOutlet weak var pregress_docDownload: UIProgressView!
    @IBOutlet weak var label_leftValue: UILabel!
    @IBOutlet weak var label_rightValue: UILabel!
    var documentsURL : URL = URL.init(fileURLWithPath: "")
    var array_Data : NSMutableArray = []
    let array_iconsData : NSArray = ["Profile", "My Book", "My Book", "Notifications", "Settings", "Music", "My Forms", "Logout"]
    let array_titleData : NSArray = ["Profile", "eBook - B", "checklist", "Reminders", "Settings", "Swedish Audio Player", "My Forms", "Logout"]
    var url_englishBook : NSString = ""
    var url_swedishBook : NSString = ""
    var list = [SKProduct]()  
    var p = SKProduct()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    LocalizeHelper.setLanguage("en")
                }
                else{
                    LocalizeHelper.setLanguage("sv")
                }
            }
        }
        
        tableView_Home.rowHeight = UITableView.automaticDimension
        tableView_Home.estimatedRowHeight = 100
        tableView_Home.tableFooterView = UIView()
        
        self.addNavigationControllerActivities()
        view_fileDownload.isHidden = true
        
        self.getBooksDownloadLinksAPICalled()
        self.setLanguageLabels()
        
        if(SKPaymentQueue.canMakePayments()) {
            print("IAP is enabled, loading")
            let productID: NSSet = NSSet(objects: Constants.iap_id)
            let request: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            request.delegate = self
            request.start()
        } else {
            print("please enable IAPS")
        }
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
        IQKeyboardManager.shared.enable = true
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
                }
                else{
                    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Klar"
                }
            }
        }
        self.setLanguageLabels()
    }
    
    func setLanguageLabels(){
        self.title = LocalizeHelper.localizedString(forKey: "Home")
        array_Data.removeAllObjects()
        for i in 0..<array_titleData.count {
            array_Data.insert(LocalizeHelper.localizedString(forKey: array_titleData[i] as? String) ?? "", at: i)
        }
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        let backButton = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationItem.leftBarButtonItem = backButton
        //       self.navigationController?.navigationBar.titleTextAttributes =
        //            [NSAttributedString.Key.foregroundColor: UIColor.black,
        //             NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        
        //        let restorePurchaseButton = UIButton()
        //        restorePurchaseButton.setTitle(LocalizeHelper.localizedString(forKey: "Restore")!, for: .normal)
        //        restorePurchaseButton.setTitleColor(UIColor.black, for: .normal)
        //        restorePurchaseButton.frame = CGRect.init(x: self.view.frame.size.width - 120, y: ((self.navigationController?.navigationBar.frame.size.height)! - 40)/2, width: 100, height: 40)
        //        restorePurchaseButton.addTarget(self, action:#selector(restorePurchaseClicked), for:.touchUpInside)
        //        self.navigationController?.navigationBar.addSubview(restorePurchaseButton)
    }
    
    //    @objc func restorePurchaseClicked(){
    //        SKPaymentQueue.default().add(self)
    //        SKPaymentQueue.default().restoreCompletedTransactions()
    //    }
    
    //MARK: API Calling...
    func getBooksDownloadLinksAPICalled(){
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/getBooks", method: .post, parameters: nil)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            let array_books = jsonDict["data"] as! NSArray
                            for index in 0..<array_books.count {
                                guard let book: NSDictionary = array_books[index] as? NSDictionary else {
                                    return
                                }
                                guard let bookLanguage : NSString = book["project_name"] as? NSString, let bookUrl :NSString = book["epub_link"] as? NSString else {
                                    
                                    return
                                }
                                if bookLanguage == "english"{
                                    self.url_englishBook = bookUrl
                                }
                                else if(bookLanguage == "swedish"){
                                    self.url_swedishBook = bookUrl
                                }
                            }
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: API Calling...
    func saveInAppPurchasePaymentStatus(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "status" : "paid"
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/savePayment", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
//                            self.openEPubBook()
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: API Calling...
    func getInAppPurchasePaymentStatus(selectedIndex:Int){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/getPayment", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            let subDict : NSDictionary! = jsonDict["data"] as? NSDictionary
                            if subDict["status"] as! String == "paid" {
                                if selectedIndex == 1 {
                                    self.openEPubBook()
                                } 
                                if selectedIndex == 2 {
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CheckListViewController") as? CheckListViewController
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }
                                if selectedIndex == 3 {
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "notificationSettingsVC") as? NotificationSettingsVC
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }
                                if selectedIndex == 5 {
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AudioListVC") as? AudioListVC
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }
                                if selectedIndex == 6 {
                                    //Check if user already logged in...
                                    let userDefaults = UserDefaults.standard
                                    let decoded = userDefaults.data(forKey: "preferredlanguage")
                                    if (decoded != nil) {
                                        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
                                        if (decodedTeams != nil) {
                                            if decodedTeams as! String == "english"{
                                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myFormsVC") as? MyFormsVC
                                                self.navigationController?.pushViewController(vc!, animated: true)
                                            }
                                            else{
                                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myFormsVC_Swedish") as? MyFormsVC_Swedish
                                                self.navigationController?.pushViewController(vc!, animated: true)
                                            }
                                        }
                                    }
                                }
                            }
                            else if subDict["status"] as! String == "expired" {
                                let alertController = UIAlertController(title: LocalizeHelper.localizedString(forKey: "Alert")!, message: LocalizeHelper.localizedString(forKey: "Your one year subscription has been completed. Do you want to purchase again?")!, preferredStyle:UIAlertController.Style.alert)
                                alertController.addAction(UIAlertAction(title: LocalizeHelper.localizedString(forKey: "Yes")!, style: UIAlertAction.Style.default)
                                { action -> Void in
                                    for product in self.list {
                                        let prodID = product.productIdentifier
                                        if(prodID == Constants.iap_id) {
                                            self.p = product
                                            self.buyProduct()
                                        }
                                    }
                                })
                                alertController.addAction(UIAlertAction(title: LocalizeHelper.localizedString(forKey: "No")!, style: UIAlertAction.Style.default)
                                { action -> Void in
                                })
                                self.present(alertController, animated: true, completion: nil)
                            }
                            else if subDict["status"] as! String == "unpaid" {
                                for product in self.list {
                                    let prodID = product.productIdentifier
                                    if(prodID == Constants.iap_id) {
                                        self.p = product
                                        self.buyProduct()
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    func logoutAPICalled(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/logout", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            self.removeFilesFromLocalStorage(languageName: "english")
                            self.removeFilesFromLocalStorage(languageName: "swedish")
                            
                            let userDefaults = UserDefaults.standard
                            userDefaults.removeObject(forKey:"loggedInUserRecord")
                            userDefaults.synchronize()
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.imageview_Icon.image = UIImage.init(named: array_iconsData[indexPath.row] as? String ?? "")
        cell.label_Title.text = array_Data[indexPath.row] as? String
        return cell
    }
    
    //MARK: UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row == 0 {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "profileVC") as? ProfileVC
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if indexPath.row == 4 {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "settingsVC") as? SettingsVC
            vc?.delegate = self
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if indexPath.row == 7 {
            self.logOut()
        }
        else {
            self.getInAppPurchasePaymentStatus(selectedIndex:indexPath.row)
        }
    }
    
    func languageChangedSuccessfully() {
        self.setLanguageLabels()
        tableView_Home.reloadData()
    }
    
    func isFileExists(languageName : String){
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("\(languageName).epub") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myBookVC") as? My_BookVC
                vc?.string_bookPath = filePath
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else {
                if languageName == "english"{
                    //English
                    let url : URL! = URL.init(string:self.url_englishBook as String)                    //"http://testdemo.website/easydigy/wp-content/uploads/anthologize-temp/c1339ceba356788cad8de6536440b6747daec5c8/book.epub")
                    self.downloadUsingAlamofire(fileUrl: url, fileName: "\(languageName).epub")
                }  
                else{
                    //Swedish
                    let url : URL! = URL.init(string:self.url_swedishBook as String)
                    self.downloadUsingAlamofire(fileUrl: url, fileName: "\(languageName).epub")
                }
                print("FILE NOT AVAILABLE")
            }
        }
        else {
            print("FILE PATH NOT AVAILABLE")
        }
    }
    
    // MARK: - DownLoad ePUB File
    func downloadUsingAlamofire(fileUrl: URL, fileName: String) {
        //AppDelegate.showHUD(inView: self.view, message:"Downloading....")
        view_fileDownload?.isHidden = false
        let manager = Alamofire.SessionManager.default
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            self.documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            self.documentsURL.appendPathComponent(fileName)
            print(self.documentsURL)
            return (self.documentsURL, [.removePreviousFile])  
        }
        manager.download(fileUrl, to: destination)
            .downloadProgress(queue: .main, closure: { (progress) in
                //progress closure
                print(progress.fractionCompleted)
                self.pregress_docDownload.progress = Float(progress.fractionCompleted)
                self.label_leftValue.text = "\(Int((progress.fractionCompleted)*100))%"
                self.label_rightValue.text = "\(Int((progress.fractionCompleted)*100))/100"
            })
            .validate { request, response, temporaryURL, destinationURL in
                //Custom evaluation closure now includes file URLs (allows you to parse out error messages if necessary)
                //DispatchQueue.main.async {
                //AppDelegate.hideHUD(inView: self.view)
                //}
                return .success
            }
            .responseData { response in
                if let destinationUrl = response.destinationURL {
                    // DispatchQueue.main.async {
                    //      AppDelegate.hideHUD(inView: self.view)
                    //  }
                    self.hidefileDownloadingView()
                    print(destinationUrl)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myBookVC") as? My_BookVC
                    vc?.string_bookPath = destinationUrl.path
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
                else {
                    self.hidefileDownloadingView()
                }
        }
    }
    
    func hidefileDownloadingView(){
        if view_fileDownload != nil{
            view_fileDownload.isHidden = true
            self.pregress_docDownload.progress = 0
            self.label_leftValue.text = "0%"
            self.label_rightValue.text = "0/100"
        }
    }
    
    func logOut(){
        guard let messageAlert  = LocalizeHelper.localizedString(forKey: "Reminder to study will not function when you are logged out!") else {
            return
        }
        let alertController = UIAlertController(title: LocalizeHelper.localizedString(forKey: "Are you sure to logout?"), message: messageAlert, preferredStyle: .alert)
        //Change Message With Color
        let messageMutableString = NSMutableAttributedString(string: messageAlert)
        messageMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:0,length:messageAlert.count))
        alertController.setValue(messageMutableString, forKey: "attributedMessage")
        
        alertController.addAction(UIAlertAction(title: LocalizeHelper.localizedString(forKey: "No"), style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        
        alertController.addAction(UIAlertAction(title: LocalizeHelper.localizedString(forKey: "Yes"), style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.logoutAPICalled()
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func removeFilesFromLocalStorage(languageName : String){
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("\(languageName).epub") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                do {
                    let _ = try fileManager.removeItem(atPath: filePath)
                    print("Deleted successfully.")
                }
                catch {
                    print("Something went wrong in file deletion.")
                }
            }
        }
        else {
            print("FILE PATH NOT AVAILABLE")
        }
        //clear audio files from document directory...
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                if fileURL.pathExtension == "mp3" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
        }
        catch  {
            print(error)
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_downloadFileCancelClicked(_ sender: UIButton) {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            downloadTasks.forEach { $0.cancel() }
            DispatchQueue.main.async {
                self.hidefileDownloadingView()
            }
        }
    }
    
    func openEPubBook() {
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                self.isFileExists(languageName: decodedTeams as! String)
            }
        }
    }
}

extension HomeVC : SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("product request")
        let myProduct = response.products
        for product in myProduct {
            print("product added")
            print(product.productIdentifier)
            print(product.localizedTitle)
            print(product.localizedDescription)
            print(product.price)
            
            list.append(product)
        }
        
        //        outRemoveAds.isEnabled = true
        //        outAddCoins.isEnabled = true
        //        outRestorePurchases.isEnabled = true
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("add payment")
        
        for transaction: AnyObject in transactions {
            let trans = transaction as! SKPaymentTransaction
            print(trans.error ?? "")
            
            switch trans.transactionState {
            case .purchased:
                print("buy ok, unlock IAP HERE")
                print(p.productIdentifier)
                queue.finishTransaction(trans)
                self.saveInAppPurchasePaymentStatus()
                break
            case .failed:
                print("buy error")
                queue.finishTransaction(trans)
                break
            case .restored:
                print("buy error")
                queue.restoreCompletedTransactions()
                break
            default:
                print("Default")
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("transactions restored")
        for transaction in queue.transactions {
            let t: SKPaymentTransaction = transaction
            let prodID = t.payment.productIdentifier as String
            
            switch prodID {
            case Constants.iap_id:
                print("Restore successfully!!!")
            default:
                for product in list {
                    let prodID = product.productIdentifier
                    if(prodID == Constants.iap_id) {
                        p = product
                        buyProduct()
                    }
                }
                print("IAP not found!!!")
            }
        }
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return true
    }
    
    func buyProduct() {
        
        print("buy " + p.productIdentifier)
        let pay = SKPayment(product: p)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(pay as SKPayment)
        
    }
}
