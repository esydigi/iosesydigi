import Alamofire

class SignUPVC: UIViewController {
    @IBOutlet weak var textField_UserName: UITextField!
    @IBOutlet weak var textField_Email: UITextField!
    @IBOutlet weak var textField_Password: UITextField!
    @IBOutlet weak var textField_ConfirmPassword: UITextField!
    @IBOutlet weak var button_Submit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Password left padding
        textField_UserName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Email.frame.height))
        textField_UserName.leftViewMode = .always
        
        //Email left padding
        textField_Email.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Email.frame.height))
        textField_Email.leftViewMode = .always
        
        //Password left padding
        textField_Password.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Email.frame.height))
        textField_Password.leftViewMode = .always
        
        //Confirm Password left padding
        textField_ConfirmPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Email.frame.height))
        textField_ConfirmPassword.leftViewMode = .always
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            textField_UserName.layer.cornerRadius = 24
            textField_Email.layer.cornerRadius = 24
            textField_Password.layer.cornerRadius = 24
            textField_ConfirmPassword.layer.cornerRadius = 24
            button_Submit.layer.cornerRadius = 24
        }
        else{
            textField_UserName.layer.cornerRadius = 20
            textField_Email.layer.cornerRadius = 20
            textField_Password.layer.cornerRadius = 20
            textField_ConfirmPassword.layer.cornerRadius = 20
            button_Submit.layer.cornerRadius = 20
        }
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_submitClicked(_ sender: UIButton) {
        if textField_UserName.text == ""{
            Utility.showAlert(message:"Ange användarnamn. / Please enter username.", controller: self)
            return
        }
        if textField_Email.text == "" || self.isValidEmail(emailStr:textField_Email.text!) == false{
            Utility.showAlert(message:"Ange giltigt e-postmeddelande. / Please enter valid email.", controller: self)
            return
        }
        if textField_Password.text == ""{
            Utility.showAlert(message:"Vänligen skriv in ett lösenord. / Please enter password.", controller: self)
            return
        }
        if textField_ConfirmPassword.text == ""{
            Utility.showAlert(message:"Ange bekräfta lösenord. / Please enter confirm password.", controller: self)
            return
        }
        if textField_Password.text != textField_ConfirmPassword.text{
            Utility.showAlert(message:"Lösenordet stämmer inte med det bekräftade lösenordet. / Password doesn't match with confirm password.", controller: self)
            return
        }
        // let uniquedeviceIdentifier : String = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let parameters : Parameters = [
            "username" : textField_UserName.text!,
            "email" : textField_Email.text!,
            "password" : textField_Password.text!,
            //"display_name" : textField_UserName.text!,
            "licensenumber" : "",
            "validity" : "",
            "language" : "swedish"
            // "deviceId" :  uniquedeviceIdentifier,
            //"pushtoken" : "drC3uyxsiKo:APA91bHKHvNXGwRwNRP0pcN9cr3BmQZ56KQa9B-s6yUOgprwa2c0wwop5Hw4cMQ8yfkJM7V8Mod2ovRh-qwdx0UeTLvn4BXcWO-sVT6nNtcuifj7xea-cCt5eU7G5Q_XQ8lSam-rRvSQ",
            //"insecure" : "cool",
            //"devicetype" : "ios",
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        //Alamofire.request(Constants.apiURL+"user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        Alamofire.request(Constants.apiURL+"user/register", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            iToast.show(jsonDict["message"] as? String)
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    @IBAction func button_signINClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
