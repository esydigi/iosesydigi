//
//  AudioListVC.swift
//  esyDigi
//
//  Created by Unyscape Infocom on 24/09/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import Alamofire
import IQKeyboardManagerSwift
import AVFoundation
import AVKit

class AudioTableViewCell: UITableViewCell{
    @IBOutlet weak var label_Title: UILabel!
}

class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.trackRect(forBounds: bounds)
        rect.size.height = 8
        return rect
    }
}

class AudioListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView_Audio: UITableView!
    @IBOutlet weak var view_fileDownload: UIView!
    @IBOutlet weak var pregress_docDownload: UIProgressView!
    @IBOutlet weak var label_leftValue: UILabel!
    @IBOutlet weak var label_rightValue: UILabel!
    var documentsURL : URL = URL.init(fileURLWithPath: "")
    var array_Data : NSArray = []

    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var songNameLbl: UILabel!
    @IBOutlet weak var songSlider: CustomSlider!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    var fileNameStr = ""
    var audioPlayer : AVAudioPlayer!
    var isPlaying = false
    @IBOutlet weak var playPauseBtn: UIButton!
    var updater : CADisplayLink! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    LocalizeHelper.setLanguage("en")
                }
                else{
                    LocalizeHelper.setLanguage("sv")
                }
            }
        }
        
        tableView_Audio.rowHeight = UITableView.automaticDimension
        tableView_Audio.estimatedRowHeight = 100
        tableView_Audio.tableFooterView = UIView()
        
        self.addNavigationControllerActivities()
        view_fileDownload.isHidden = true
        playerView.isHidden = true
//        songSlider.addTarget(self, action: #selector(AudioListVC.changeVlaue(_:)), for: .valueChanged)

        self.getAudioLinksAPICalled()
        self.setLanguageLabels()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.delegate = self
        playerView.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
//          let backButton = UIBarButtonItem(title: "< Home", style: .plain, target: self, action: #selector(backClicked))
//           navigationItem.leftBarButtonItem = backButton
//        self.navigationController?.navigationBar.titleTextAttributes =
//          [NSAttributedString.Key.foregroundColor: UIColor.black,
//           NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
    }
    
    @objc func backClicked(){
        if isPlaying == true {
            isPlaying = false 
            audioPlayer.stop()
        }
        if updater != nil {
            updater.invalidate()
        }
        isPlaying = false 
        self.navigationController?.popViewController(animated: true)
    }
    
    func setLanguageLabels(){
        self.title = LocalizeHelper.localizedString(forKey: "Audio List")
    }
    
    //MARK: API Calling...
    func getAudioLinksAPICalled(){
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/book_with_mp3/", method: .post, parameters: nil)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            self.array_Data = jsonDict["data"] as! NSArray
                            self.tableView_Audio.reloadData()
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell", for: indexPath) as! AudioTableViewCell
        let audio: NSDictionary = (array_Data[indexPath.row] as? NSDictionary)!
        let titleStr : String = (audio.value(forKeyPath: "title") as? String)!
        cell.label_Title.text = titleStr.capitalized
        return cell
    }
    
    //MARK: UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let audio: NSDictionary = (array_Data[indexPath.row] as? NSDictionary)!
        let url = audio.value(forKeyPath: "mp3_url") as? String
        if url == "" {
            DispatchQueue.main.async {
                Utility.showAlert(message: LocalizeHelper.localizedString(forKey: "No Audio Found.") ?? "", alertMessage: LocalizeHelper.localizedString(forKey: "Alert") ?? "", controller: self)
            }
        }
        else {
            self.isFileExists(audioUrl: url!)
        }
    }
    
    func isFileExists(audioUrl : String){
        fileNameStr = audioUrl.components(separatedBy: "/").last!
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileNameStr) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
//                do {
//                    player = try AVAudioPlayer(contentsOf:URL(fileURLWithPath: filePath))
//                    //player!.numberOfLoops = -1
//                    player!.prepareToPlay()
//                    player!.play()
//
//                    let playerViewController = AVPlayerViewController()
//                    playerViewController.player = Av
//                    self.present(playerViewController, animated: true) {
//                        playerViewController.player!.play()
//                    }
//                }
//                catch let error as NSError {
//                    print(error.description)
//                }
//                let playerViewController = AVPlayerViewController()
//                playerViewController.player = AVPlayer(url: URL(fileURLWithPath: filePath))
//                self.present(playerViewController, animated: true) {
//                    playerViewController.player!.play()
//                    playerViewController.exitsFullScreenWhenPlaybackEnds = true
//               }
//                let playerController = AVPlayerViewController()
//                playerController.player = AVPlayer(url: URL(fileURLWithPath: filePath))
//                self.addChild(playerController)
//                playerController.view.frame = CGRect.init(x: 0, y: 200, width: self.view.frame.size.width, height: 200)
//                self.view.addSubview(playerController.view)
                self.playSong(url:URL(fileURLWithPath: filePath))
            }
            else {
                let url : URL! = URL.init(string: audioUrl)
                self.downloadUsingAlamofire(fileUrl: url, fileName: fileNameStr)
                print("FILE NOT AVAILABLE")
            }
        }
        else {
            print("FILE PATH NOT AVAILABLE")
        }
    }
    
    // MARK: - DownLoad ePUB File
    func downloadUsingAlamofire(fileUrl: URL, fileName: String) {
        //AppDelegate.showHUD(inView: self.view, message:"Downloading....")
        view_fileDownload?.isHidden = false
        let manager = Alamofire.SessionManager.default
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            self.documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            self.documentsURL.appendPathComponent(fileName)
            print(self.documentsURL)
            return (self.documentsURL, [.removePreviousFile])
        }
        manager.download(fileUrl, to: destination)
            .downloadProgress(queue: .main, closure: { (progress) in
                //progress closure
                print(progress.fractionCompleted)
                self.pregress_docDownload.progress = Float(progress.fractionCompleted)
                self.label_leftValue.text = "\(Int((progress.fractionCompleted)*100))%"
                self.label_rightValue.text = "\(Int((progress.fractionCompleted)*100))/100"
            })
            .validate { request, response, temporaryURL, destinationURL in
                //Custom evaluation closure now includes file URLs (allows you to parse out error messages if necessary)
                //DispatchQueue.main.async {
                //AppDelegate.hideHUD(inView: self.view)
                //}
                return .success
            }
            .responseData { response in
                if let destinationUrl = response.destinationURL {
                    // DispatchQueue.main.async {
                    //      AppDelegate.hideHUD(inView: self.view)
                    //  }
                    self.hidefileDownloadingView()
                    print(destinationUrl)
                    self.playSong(url:destinationUrl)
                }
                else {
                    self.hidefileDownloadingView()
                }
        }
    }
    
    func hidefileDownloadingView(){
        if view_fileDownload != nil{
            view_fileDownload.isHidden = true
            self.pregress_docDownload.progress = 0
            self.label_leftValue.text = "0%"
            self.label_rightValue.text = "0/100"
        }
    }
    
    func removeFilesFromLocalStorage(languageName : String){
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("\(languageName).epub") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                do {
                    let _ = try fileManager.removeItem(atPath: filePath)
                    print("Deleted successfully.")
                }
                catch {
                    print("Something went wrong in file deletion.")
                }
            }
        }
        else {
            print("FILE PATH NOT AVAILABLE")
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_downloadFileCancelClicked(_ sender: UIButton) {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            downloadTasks.forEach { $0.cancel() }
            DispatchQueue.main.async {
                self.hidefileDownloadingView()
            }
        }
    }
    
    func playSong(url:URL) {
        self.playerView.isHidden = false
        self.songNameLbl.text = fileNameStr.capitalized

        updater = CADisplayLink(target: self, selector: #selector(trackAudioProgress))
        updater.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf:url)
            audioPlayer.numberOfLoops = -1
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            isPlaying = true
            playPauseBtn.setImage(UIImage.init(named: "pauseIcon"), for: .normal)
        }
        catch let error as NSError {
            print(error.description)
        }
    }
    
    @IBAction func crossBtnTap(_ sender: Any) {
        audioPlayer.stop()
        updater.invalidate()
        self.playerView.isHidden = true
    }
 
    @IBAction func playPauseBtnTap(_ sender: Any) {
        if isPlaying {
            audioPlayer.pause()
            isPlaying = false
            playPauseBtn.setImage(UIImage.init(named: "playIcon"), for: .normal)
        }
        else {
            audioPlayer.play()
            isPlaying = true
            playPauseBtn.setImage(UIImage.init(named: "pauseIcon"), for: .normal)
        }
    }
    
//    @objc func changeVlaue(_ sender: UISlider) {
//        audioPlayer.currentTime = audioPlayer.duration * Double(sender.value)
//        print("value is" , sender.value);
//    }
    
    @objc func trackAudioProgress() {
        if audioPlayer != nil {
            let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
            songSlider.value = normalizedTime / 100
            
            let totalTime = Time(sec: Float(audioPlayer.duration))
            let currentTime = Time(sec: Float(audioPlayer.currentTime))
            
            startTimeLbl.text = currentTime!.minAndSec
            endTimeLbl.text = totalTime!.minAndSec
            
            if startTimeLbl.text == endTimeLbl.text {
                audioPlayer.stop()
                updater.invalidate()
                self.playerView.isHidden = true
            }
        }
        else {
            self.playerView.isHidden = true
        }
    }
}

extension AudioListVC : AVAudioPlayerDelegate {
    internal func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        audioPlayer.stop()
        updater.invalidate()
        self.playerView.isHidden = true
    }
}

extension AudioListVC: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        if sender?.view == self.playerView {
            audioPlayer.stop()
            updater.invalidate()
            self.playerView.isHidden = true
        }
    }
}
