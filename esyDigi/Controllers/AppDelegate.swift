import UIKit
import IQKeyboardManagerSwift
import Reachability
import MBProgressHUD
import FirebaseCore
import FirebaseMessaging
import Firebase
import UserNotifications
import AVFoundation
import MediaPlayer
import Alamofire

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate,MessagingDelegate {
    static let shared = AppDelegate()
    var window: UIWindow?
    //Orientation Variables
    var myOrientation: UIInterfaceOrientationMask = .portrait
    var isUserBookOpen = false
    let reachability = Reachability()!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //   let navigationBarAppearace = UINavigationBar.appearance()
        //  navigationBarAppearace.tintColor = UIColor.black
        
        //        let xyz : String? = "dgerger"
        //        guard let ab1 = xyz else {
        //
        //            guard let cbv = xyz else {
        //
        //            }
        //        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [.mixWithOthers, .allowAirPlay])
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        }
        catch {
            print(error)
        }    
        
        self.setupFireBase(application: application)
        self.setupReachability()
        IQKeyboardManager.shared.enable = true
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
                }
                else{
                    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Klar"
                }
            }
        }
        return true
    }
    
    // MARK: SETUP FIREBASE
    func setupFireBase(application:UIApplication){
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        //        //get application instance ID
        //        InstanceID.instanceID().instanceID { (result, error) in
        //            if error != nil {
        //                print("Error fetching remote instance ID: (error)")
        //            } else if result != nil {
        //                print("Remote instance ID token: \(result?.token)")
        //            }
        //        }
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
    }
    
    //MARK: APNS Delegates
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Successfully registered for notifications!")
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: (messageID)")
        //        }
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: (error.localizedDescription)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
        
    }
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        completionHandler()
    }
    
    //MARK: FCM Delegates
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let userDefaults = UserDefaults.standard
        userDefaults.set(fcmToken, forKey: "fcmToken")
        //let dataDict:[String: String] = ["token": fcmToken]
        //  NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        //Note: This callback is fired at each app startup and whenever a new token is generated.
        
        let decoded = UserDefaults.standard.data(forKey: "loggedInUserRecord")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                loggedInUser.shared.initializeUserDetails(dicuserdetail: decodedTeams as! Dictionary<String, Any>)
                self.updateFCMAPI(tokenKey: fcmToken)
            }  
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: (remoteMessage.appData)")
        iToast.show(remoteMessage.description)
    }
    
    //MARK: UIInterfaceOrientation
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if isUserBookOpen {
            myOrientation = .all
        }
        return  myOrientation
    }
    
    // MARK: REACHABILITY
    func setupReachability(){
        do {
            try reachability.startNotifier()
        }
        catch {
            print("Unable to start notifier")
        }
        
        //declare this inside of viewWillAppear
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }
        catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
        case .cellular:
            print("Reachable via Cellular")
        case .none:
            iToast.show("Network is not available")
            print("Network not reachable")
        }
    }
    
    //MARK: MBProgressHUD
    class func showHUD(inView:UIView, message:String){
        let loadingNotification = MBProgressHUD.showAdded(to: inView, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = message
    }
    
    class func hideHUD(inView:UIView){
        MBProgressHUD.hide(for: inView, animated: true)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

extension AppDelegate {
    func updateFCMAPI(tokenKey: String) {
        let uniquedeviceIdentifier : String = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "deviceId" :  uniquedeviceIdentifier,
            "pushtoken" : tokenKey,
            "devicetype" : "ios"
        ]
        Alamofire.request(Constants.apiURL+"user/updateUserToken", method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "success"){
                            
                        }
                        else{
//                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error): break
//                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
}
