import Alamofire

class LoginVC : UIViewController {
    
    @IBOutlet weak var textField_UserName: UITextField!
    @IBOutlet weak var textField_Password: UITextField!
    @IBOutlet weak var button_Submit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Code for Email left padding
        textField_UserName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_UserName.frame.height))
        textField_UserName.leftViewMode = .always
        
        //Code for Password left padding
        textField_Password.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_UserName.frame.height))
        textField_Password.leftViewMode = .always
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            textField_UserName.layer.cornerRadius = 24
            textField_Password.layer.cornerRadius = 24
            button_Submit.layer.cornerRadius = 24
        }
        else {
            textField_UserName.layer.cornerRadius = 20
            textField_Password.layer.cornerRadius = 20
            button_Submit.layer.cornerRadius = 20
        }
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "loggedInUserRecord")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                loggedInUser.shared.initializeUserDetails(dicuserdetail: decodedTeams as! Dictionary<String, Any>)
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "homeVC") as? HomeVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        //Disable swipe gesture for popviewcontroller...
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    //    override var shouldAutorotate: Bool {
    //        return true
    //    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_submitClicked(_ sender: UIButton) {
        //        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "homeVC") as? HomeVC
        //        self.navigationController?.pushViewController(vc!, animated: true)
        //        return
        //
        
        if textField_UserName.text == ""{
            Utility.showAlert(message:"Ange användarnamn. / Please enter username.", controller: self)
            return
        }
        
        if textField_Password.text == ""{
            Utility.showAlert(message:"Vänligen skriv in ett lösenord. / Please enter password.", controller: self)
            return
        }
        
        let uniquedeviceIdentifier : String = UIDevice.current.identifierForVendor?.uuidString ?? ""
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let fcmToken : String = userDefaults.value(forKey:"fcmToken") as? String ?? ""
        let parameters : Parameters = [
            "username" : textField_UserName.text!,
            "password" : textField_Password.text!,
            "deviceId" :  uniquedeviceIdentifier,
            "pushtoken" : fcmToken,
            "devicetype" : "ios"
        ]
        AppDelegate.showHUD(inView: self.view, message:"")
        Alamofire.request(Constants.apiURL+"user/login", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:  
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            let userDefaults = UserDefaults.standard 
                            let dataUser = jsonDict["user"] as! NSDictionary
                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dataUser)
                            userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                            userDefaults.synchronize()
                            loggedInUser.shared.initializeUserDetails(dicuserdetail: dataUser as! Dictionary<String, Any>)
                            self.textField_UserName.text = ""
                            self.textField_Password.text = ""
                            
                            let userDefaultslang = UserDefaults.standard
                            let encodedDatalang: Data = NSKeyedArchiver.archivedData(withRootObject: dataUser["prefferedlanguage"] as! String)
                            userDefaultslang.set(encodedDatalang, forKey: "preferredlanguage")
                            userDefaultslang.synchronize()
                            
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "homeVC") as? HomeVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    @IBAction func button_signUPClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "signUPVC") as? SignUPVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func button_lostYourPassword(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "forgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
