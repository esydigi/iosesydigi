import Alamofire
import UIKit

protocol changeLanguageDelegate {
    func languageChangedSuccessfully()
}

class SettingsVC: UIViewController{
    @IBOutlet weak var label_Brightness: UILabel!
    @IBOutlet weak var slider_Brightness: UISlider!
    @IBOutlet weak var label_ChooseLanguage: UILabel!
    @IBOutlet weak var imageview_English: UIImageView!
    @IBOutlet weak var imageview_swedish: UIImageView!
    
    var delegate : changeLanguageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigationControllerActivities()
        //self.getprefferedlanguageAPICalled()
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    self.setRadioButton(language: "english")
                }
                else{
                    self.setRadioButton(language: "swedish")
                }
            }
        }
        self.setLanguageLabels()
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //        let backButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(buttonSaveClicked))
        //        navigationItem.rightBarButtonItem = backButton
        
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setLanguageLabels(){
        self.title = LocalizeHelper.localizedString(forKey: "Settings")
        label_Brightness.text = LocalizeHelper.localizedString(forKey: "Brightness")
        label_ChooseLanguage.text = LocalizeHelper.localizedString(forKey: "Choose Language")
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
        self.slider_Brightness.value = Float(UIScreen.main.brightness)
    }
    
    @IBAction func slider_valueChanged(_ sender: UISlider) {
        UIScreen.main.brightness = CGFloat(sender.value)
    }
    
    //MARK: API Calling...
    func getprefferedlanguageAPICalled(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/getprefferedlanguage", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            let language = jsonDict["prefferedlanguage"]
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(language, forKey: "preferredlanguage")
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    func setprefferedlanguageAPICalled(language: NSString){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "prefferedlanguage" : language
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/prefferedlanguage", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            iToast.show(jsonDict["message"] as? String)
                            let userDefaults = UserDefaults.standard
                            let encodedDataLang: Data = NSKeyedArchiver.archivedData(withRootObject: language)
                            userDefaults.set(encodedDataLang, forKey: "preferredlanguage")
                            userDefaults.synchronize()
                            
                            if language == "english"{
                                LocalizeHelper.setLanguage("en")
                            }
                            else{
                                LocalizeHelper.setLanguage("sv")
                            }
                            self.setLanguageLabels()
                            self.delegate?.languageChangedSuccessfully()
                        }
                        else{
                            iToast.show(jsonDict["message"] as? String)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_notificationSettingsClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "notificationSettingsVC") as? NotificationSettingsVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func button_englishClicked(_ sender: UIButton) {
        self.setRadioButton(language: "english")
        self.setprefferedlanguageAPICalled(language:"english")
    }
    
    @IBAction func button_swedishClicked(_ sender: UIButton) {
        self.setRadioButton(language: "swedish")
        self.setprefferedlanguageAPICalled(language:"swedish")
    }
    
    func setRadioButton(language : NSString) {
        if language == "english"{
            imageview_English.image = UIImage(named: "radio-on")
            imageview_swedish.image = UIImage.init(named: "radio-off")
        }
        else{
            imageview_English.image = UIImage.init(named: "radio-off")
            imageview_swedish.image = UIImage.init(named: "radio-on")
        }
    }
}
