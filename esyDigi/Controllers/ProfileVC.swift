import Alamofire

class ProfileVC: UIViewController {
    @IBOutlet weak var label_UserName: UILabel!
    @IBOutlet weak var textField_Email: UITextField!
    @IBOutlet weak var textField_LicenseNumber: UITextField!
    @IBOutlet weak var textField_Validity: UITextField!
    @IBOutlet weak var button_Submit: UIButton!
    @IBOutlet weak var button_ChangePassword: UIButton!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        //Code for Email left padding
        textField_Email.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Email.frame.height))
        textField_Email.leftViewMode = .always
        
        //Code for License Number left padding
        textField_LicenseNumber.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_LicenseNumber.frame.height))
        textField_LicenseNumber.leftViewMode = .always
        
        //Code for Validity left padding
        textField_Validity.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Validity.frame.height))
        textField_Validity.leftViewMode = .always
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            textField_Email.layer.cornerRadius = 24
            textField_LicenseNumber.layer.cornerRadius = 24
            textField_Validity.layer.cornerRadius = 24
            button_Submit.layer.cornerRadius = 24
            button_ChangePassword.layer.cornerRadius = 24
        }
        else{
            textField_Email.layer.cornerRadius = 20
            textField_LicenseNumber.layer.cornerRadius = 20
            textField_Validity.layer.cornerRadius = 20
            button_Submit.layer.cornerRadius = 20
            button_ChangePassword.layer.cornerRadius = 20
        }
        
        self.addNavigationControllerActivities()
        
        //Fetch user profile...
        self.getUserProfile()
        
        self.label_UserName.text = "\(LocalizeHelper.localizedString(forKey: "User Name")!) :"
        self.textField_Email.placeholder = LocalizeHelper.localizedString(forKey: "Email")
        self.textField_LicenseNumber.placeholder = LocalizeHelper.localizedString(forKey: "License Number")
        self.textField_Validity.placeholder = LocalizeHelper.localizedString(forKey: "Validity")
        button_Submit.setTitle(LocalizeHelper.localizedString(forKey: "Submit"), for: .normal)
        button_ChangePassword.setTitle(LocalizeHelper.localizedString(forKey: "Change Password"), for: .normal)        
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //  let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backClicked))
        //   navigationItem.leftBarButtonItem = backButton
        //self.navigationController?.navigationBar.titleTextAttributes =
        //  [NSAttributedString.Key.foregroundColor: UIColor.black,
        //   NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        self.title = LocalizeHelper.localizedString(forKey: "Profile")
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: API Calling
    func getUserProfile(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/get_userinfo", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            //Set All fields as already filled with logged in user record...
                            let username : String = jsonDict["username"] as? String ?? ""
                            self.label_UserName.text = "\(LocalizeHelper.localizedString(forKey: "User Name")!) : " + username
                            self.textField_Email.text = jsonDict["email"] as? String
                            self.textField_LicenseNumber.text = jsonDict["license"] as? String
                            self.textField_Validity.text = jsonDict["validity"] as? String
                        }
                        else if(jsonDict["status"] as! String == "error"){
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func dp(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_Validity.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func button_submitClicked(_ sender: UIButton) {
        if textField_Email.text == "" || self.isValidEmail(emailStr:textField_Email.text!) == false{
            
            Utility.showAlert(message:LocalizeHelper.localizedString(forKey: "Please enter valid email.")!,  alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "user_email" : textField_Email.text!,
            "display_name" : "",
            "licensenumber" : textField_LicenseNumber.text!,
            "validity" : textField_Validity.text!,
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/update_profile", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                        else if(jsonDict["status"] as! String == "error"){
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
    
    @IBAction func button_changePasswordClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "changePasswordVC") as? ChangePasswordVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

