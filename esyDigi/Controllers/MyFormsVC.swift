import Alamofire

class MyFormsVC: UIViewController {
    //My Learners Permit Section...
    @IBOutlet weak var textField_MyLearnerPermit: UITextField!
    //My Learners Permit Section...
    @IBOutlet weak var textField_MyAccompanyingDriver: UITextField!
    
    @IBOutlet weak var textField_MyAccompanyingDriver1: UITextField!
    @IBOutlet weak var textField_ValidUntil1: UITextField!
    @IBOutlet weak var textField_Approved1: UITextField!
    
    @IBOutlet weak var textField_MyAccompanyingDriver2: UITextField!
    @IBOutlet weak var textField_ValidUntil2: UITextField!
    @IBOutlet weak var textField_Approved2: UITextField!
    
    @IBOutlet weak var textField_MyAccompanyingDriver3: UITextField!
    @IBOutlet weak var textField_ValidUntil3: UITextField!
    @IBOutlet weak var textField_Approved3: UITextField!
    //Course Review date
    @IBOutlet weak var dateTextField: UITextField!
    //special condition section
    @IBOutlet weak var drivingDarkCheckBoxBtn: UIButton!
    @IBOutlet weak var slipperyRoadCheckBoxBtn: UIButton!
    @IBOutlet weak var specialConditionCommentTextField: UITextField!
    //vehicle handling section
    @IBOutlet weak var safetyCheckBoxBtn: UIButton!
    @IBOutlet weak var parkingCheckBoxBtn: UIButton!
    @IBOutlet weak var reversingCheckBoxBtn: UIButton!
    @IBOutlet weak var hillStartsCheckBoxBtn: UIButton!
    @IBOutlet weak var userOfButtonCheckBoxBtn: UIButton!
    @IBOutlet weak var brakingCheckBoxBtn: UIButton!
    @IBOutlet weak var vehicleHandlingCommentTextField: UITextField!
    //build up section
    @IBOutlet weak var residentailAreasCheckBoxBtn: UIButton!
    @IBOutlet weak var pedestrianCheckBoxBtn: UIButton!
    @IBOutlet weak var changingCheckBoxBtn: UIButton!
    @IBOutlet weak var streetCheckBoxBtn: UIButton!
    @IBOutlet weak var signalCheckBoxBtn: UIButton!
    @IBOutlet weak var roundaboutsCheckBoxBtn: UIButton!
    @IBOutlet weak var passingCheckBoxBtn: UIButton!
    @IBOutlet weak var builtUpCommentTextField: UITextField!
    //country code section
    @IBOutlet weak var drivingCheckBoxBtn: UIButton!
    @IBOutlet weak var enteringCheckBoxBtn: UIButton!
    @IBOutlet weak var turningLeftCheckBoxBtn: UIButton!
    @IBOutlet weak var turningRightCheckBoxBtn: UIButton!
    @IBOutlet weak var overtakingCheckBoxBtn: UIButton!
    @IBOutlet weak var motowaysRightCheckBoxBtn: UIButton!
    @IBOutlet weak var countryRoadCommentTextField: UITextField!
    //build up and country code section
    @IBOutlet weak var turningAroundCheckBoxBtn: UIButton!
    @IBOutlet weak var railwayCheckBoxBtn: UIButton!
    @IBOutlet weak var unprotectedCheckBoxBtn: UIButton!
    @IBOutlet weak var followingCheckBoxBtn: UIButton!
    @IBOutlet weak var roadworksCheckBoxBtn: UIButton!
    @IBOutlet weak var buildUpCountryRoadsCommentTextField: UITextField!
    //instructions section
    @IBOutlet weak var vehicleAwarenessTextField: UITextField!
    @IBOutlet weak var manoeuvringTextField: UITextField!
    @IBOutlet weak var environmentTextField: UITextField!
    @IBOutlet weak var roadTrafficTextField: UITextField!
    @IBOutlet weak var scanningRoutinesTextField: UITextField!
    @IBOutlet weak var safetyMarginsTextField: UITextField!
    @IBOutlet weak var adaptingTextField: UITextField!
    @IBOutlet weak var speedTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        self.addNavigationControllerActivities()
        //MyLearnerPermit left padding
        //        textField_MyLearnerPermit.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_MyLearnerPermit.frame.height))
        //        textField_MyLearnerPermit.leftViewMode = .always
        //
        //        //MyAccompanyingDriver left padding
        //        textField_MyAccompanyingDriver.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_MyAccompanyingDriver.frame.height))
        //        textField_MyAccompanyingDriver.leftViewMode = .always
        //Get All Form Value to fill...
        self.getAllFormValue()
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backClicked))
        //        navigationItem.leftBarButtonItem = backButton
        //
        //       self.navigationController?.navigationBar.titleTextAttributes =
        //            [NSAttributedString.Key.foregroundColor: UIColor.black,
        //             NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        
        //        let backButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(buttonSaveClicked))
        //        navigationItem.rightBarButtonItem = backButton
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 20)
        button.addTarget(self, action: #selector(buttonSaveClicked), for: .touchUpInside)
        button.setTitle("Save", for: .normal)
        button.layer.backgroundColor = UIColor.init(red: 0/255, green: 100/255, blue: 0/255, alpha: 1.0).cgColor
        button.layer.cornerRadius = 5.0
        
        let buttonItem = UIBarButtonItem(customView: button)
        toolbarItems = [buttonItem]
        
        navigationItem.rightBarButtonItem = buttonItem
        self.title = "My Forms"
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonSaveClicked(){
        if textField_MyLearnerPermit.text == "" {
            Utility.showAlert(message: LocalizeHelper.localizedString(forKey: "Please enter learner's permit valid date.")!, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        if textField_MyAccompanyingDriver.text == "" {
            Utility.showAlert(message: LocalizeHelper.localizedString(forKey: "Please enter accompanying driver valid date.")!, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        if dateTextField.text == "" {
            Utility.showAlert(message: LocalizeHelper.localizedString(forKey: "Please enter course review date.")!, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        
        let driver1 : NSArray = ["\(textField_MyAccompanyingDriver1.text!)","\(textField_ValidUntil1.text!)","\(textField_Approved1.text!)"]
        let driver2 : NSArray = ["\(textField_MyAccompanyingDriver2.text!)","\(textField_ValidUntil2.text!)","\(textField_Approved2.text!)"]
        let driver3 : NSArray = ["\(textField_MyAccompanyingDriver3.text!)","\(textField_ValidUntil3.text!)","\(textField_Approved3.text!)"]
        let special_condition : NSArray = [
            drivingDarkCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            slipperyRoadCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            specialConditionCommentTextField.text!]
        
        let vehicle_handling : NSArray = [
            safetyCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            parkingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            reversingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            hillStartsCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            userOfButtonCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            brakingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            "\(vehicleHandlingCommentTextField.text!)"]
        
        let build_up_areas : NSArray = [
            residentailAreasCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            pedestrianCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            changingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            streetCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            signalCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            roundaboutsCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            passingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            "\(builtUpCommentTextField.text!)"]
        
        let country_roads : NSArray = [
            drivingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            enteringCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            turningLeftCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            turningRightCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            overtakingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            motowaysRightCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            "\(countryRoadCommentTextField.text!)"]
        
        let build_up_areas_and_country_roads : NSArray = [
            turningAroundCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            railwayCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            unprotectedCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            followingCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            roadworksCheckBoxBtn.currentImage == #imageLiteral(resourceName: "checkBoxSelected") ? "1" : "0",
            "\(buildUpCountryRoadsCommentTextField.text!)"]
        
        let the_instructor_overall_assesment : NSArray = [
            "\(vehicleAwarenessTextField.text!)",
            "\(manoeuvringTextField.text!)",
            "\(environmentTextField.text!)",
            "\(roadTrafficTextField.text!)",
            "\(scanningRoutinesTextField.text!)",
            "\(safetyMarginsTextField.text!)",
            "\(adaptingTextField.text!)",
            "\(speedTextField.text!)"]
        
        //        var string_Time : NSString = ""
        //        if let data2 = try?  JSONSerialization.data(withJSONObject: array_AlternateDrivers, options: []){
        //            string_Time = NSString(data: data2, encoding: String.Encoding.utf8.rawValue) ?? ""
        //        }
        
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "learner_valid_date" : textField_MyLearnerPermit.text!,
            "accompanying_valid_date" : textField_MyAccompanyingDriver.text!,
            "alternate_driver1" : driver1,
            "alternate_driver2" : driver2,
            "alternate_driver3" : driver3,
            "course_review_date" : dateTextField.text!,
            "special_condition" : special_condition,
            "vehicle_handeling" : vehicle_handling,
            "built_up_areas" : build_up_areas,
            "country_roads" : country_roads,
            "builtup_areas_and_country_roads" : build_up_areas_and_country_roads,
            "the_instructor_overall_assesment" : the_instructor_overall_assesment
        ]
        print(parameters)
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/create_accompanying_driver", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            iToast.show(jsonDict["message"] as? String)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: My LearnerPermit Date
    @IBAction func MyLearnerPermitDate(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker_MyLearnerPermitDate(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker_MyLearnerPermitDate(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_MyLearnerPermit.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: My Accompanying Driver Date
    @IBAction func MyAccompanyingDriver(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker_MyAccompanyingDriver(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker_MyAccompanyingDriver(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_MyAccompanyingDriver.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: My ValidUntil1 Date
    @IBAction func ValidUntil1(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker_ValidUntil1(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker_ValidUntil1(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_ValidUntil1.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: My ValidUntil2 Date
    @IBAction func ValidUntil2(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker_ValidUntil2(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker_ValidUntil2(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_ValidUntil2.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: My ValidUntil3 Date
    @IBAction func ValidUntil3(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker_ValidUntil3(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker_ValidUntil3(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField_ValidUntil3.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: My ValidUntil3 Date
    @IBAction func dateBtn(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date.init()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(dateBtn(sender:)), for: .valueChanged)
    }
    
    @objc func dateBtn(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func checkBoxesButtonsTap(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "checkBoxUnselected") {
            sender.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal)
        }
        else {
            sender.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
        }
    }
    
    //MARK: API Calling...
    func getAllFormValue(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/get_accompanying_driver", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            //  Utility.showAlert(message: "No record found.", controller: self)
                            self.textField_MyLearnerPermit.text = jsonDict["learner_valid_date"] as? String
                            self.textField_MyAccompanyingDriver.text = jsonDict["accompanying_valid_date"] as? String
                            self.dateTextField.text = jsonDict["course_review_date"] as? String
                            let array_0 : NSArray = jsonDict["alternate_driver1"] as? NSArray ?? []
                            for index in 0..<array_0.count{
                                switch (index){
                                case 0:
                                    self.textField_MyAccompanyingDriver1.text = array_0[index] as? String
                                case 1:
                                    self.textField_ValidUntil1.text = array_0[index] as? String
                                case 2:
                                    self.textField_Approved1.text = array_0[index] as? String
                                default: break
                                }
                            }
                            let array_1 : NSArray = jsonDict["alternate_driver2"] as? NSArray ?? []
                            for index in 0..<array_1.count{
                                switch (index){
                                case 0:
                                    self.textField_MyAccompanyingDriver2.text = array_1[index] as? String
                                case 1:
                                    self.textField_ValidUntil2.text = array_1[index] as? String
                                case 2:
                                    self.textField_Approved2.text = array_1[index] as? String
                                default: break
                                }
                            }
                            let array_2 : NSArray = jsonDict["alternate_driver3"] as? NSArray ?? []
                            for index in 0..<array_2.count{
                                switch (index){
                                case 0:
                                    self.textField_MyAccompanyingDriver3.text = array_2[index] as? String
                                case 1:
                                    self.textField_ValidUntil3.text = array_2[index] as? String
                                case 2:
                                    self.textField_Approved3.text = array_2[index] as? String
                                default: break
                                }
                            }
                            let array_3 : NSArray = jsonDict["special_condition"] as? NSArray ?? []
                            for index in 0..<array_3.count{
                                switch (index){
                                case 0:
                                    array_3[index] as? String == "1" ? self.drivingDarkCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.drivingDarkCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 1:
                                    array_3[index] as? String == "1" ? self.slipperyRoadCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.slipperyRoadCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 2:
                                    self.specialConditionCommentTextField.text = array_3[index] as? String
                                default: break
                                }
                            }
                            let array_4 : NSArray = jsonDict["vehicle_handeling"] as? NSArray ?? []
                            for index in 0..<array_4.count{
                                switch (index){
                                case 0:
                                    array_4[index] as? String == "1" ? self.safetyCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.safetyCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 1:
                                    array_4[index] as? String == "1" ? self.parkingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.parkingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 2:
                                    array_4[index] as? String == "1" ? self.reversingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.reversingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 3:
                                    array_4[index] as? String == "1" ? self.hillStartsCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.hillStartsCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 4:
                                    array_4[index] as? String == "1" ? self.userOfButtonCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.userOfButtonCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 5:
                                    array_4[index] as? String == "1" ? self.brakingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.brakingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 6:
                                    self.vehicleHandlingCommentTextField.text = array_4[index] as? String
                                default: break
                                }
                            }
                            let array_5 : NSArray = jsonDict["built_up_areas"] as? NSArray ?? []
                            for index in 0..<array_5.count{
                                switch (index){
                                case 0:
                                    array_5[index] as? String == "1" ? self.residentailAreasCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.residentailAreasCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 1:
                                    array_5[index] as? String == "1" ? self.pedestrianCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.pedestrianCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 2:
                                    array_5[index] as? String == "1" ? self.changingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.changingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 3:
                                    array_5[index] as? String == "1" ? self.streetCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.streetCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 4:
                                    array_5[index] as? String == "1" ? self.signalCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.signalCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 5:
                                    array_5[index] as? String == "1" ? self.roundaboutsCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.roundaboutsCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 6:
                                    array_5[index] as? String == "1" ? self.passingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.passingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 7:
                                    self.builtUpCommentTextField.text = array_5[index] as? String
                                default: break
                                }
                            }
                            let array_6 : NSArray = jsonDict["country_roads"] as? NSArray ?? []
                            for index in 0..<array_6.count{
                                switch (index){
                                case 0:
                                    array_6[index] as? String == "1" ? self.drivingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.drivingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 1:
                                    array_6[index] as? String == "1" ? self.enteringCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.enteringCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 2:
                                    array_6[index] as? String == "1" ? self.turningLeftCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.turningLeftCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 3:
                                    array_6[index] as? String == "1" ? self.turningRightCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.turningRightCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 4:
                                    array_6[index] as? String == "1" ? self.overtakingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.overtakingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 5:
                                    array_6[index] as? String == "1" ? self.motowaysRightCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.motowaysRightCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 6:
                                    self.countryRoadCommentTextField.text = array_6[index] as? String
                                default: break
                                }
                            }
                            let array_7 : NSArray = jsonDict["builtup_areas_and_country_roads"] as? NSArray ?? []
                            for index in 0..<array_7.count{
                                switch (index){
                                case 0:
                                    array_7[index] as? String == "1" ? self.turningAroundCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.turningAroundCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 1:
                                    array_7[index] as? String == "1" ? self.railwayCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.railwayCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 2:
                                    array_7[index] as? String == "1" ? self.unprotectedCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.unprotectedCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 3:
                                    array_7[index] as? String == "1" ? self.followingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.followingCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 4:
                                    array_7[index] as? String == "1" ? self.roadworksCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal) : self.roadworksCheckBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
                                case 5:
                                    self.buildUpCountryRoadsCommentTextField.text = array_7[index] as? String
                                default: break
                                }
                            }
                            let array_8 : NSArray = jsonDict["the_instructor_overall_assesment"] as? NSArray ?? []
                            for index in 0..<array_8.count{
                                switch (index){
                                case 0:
                                    self.vehicleAwarenessTextField.text = array_8[index] as? String
                                case 1:
                                    self.manoeuvringTextField.text = array_8[index] as? String
                                case 2:
                                    self.environmentTextField.text = array_8[index] as? String
                                case 3:
                                    self.roadTrafficTextField.text = array_8[index] as? String
                                case 4:
                                    self.scanningRoutinesTextField.text = array_8[index] as? String
                                case 5:
                                    self.safetyMarginsTextField.text = array_8[index] as? String
                                case 6:
                                    self.adaptingTextField.text = array_8[index] as? String
                                case 7:
                                    self.speedTextField.text = array_8[index] as? String
                                default: break
                                }
                            }
                        }
                        else {
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
}
