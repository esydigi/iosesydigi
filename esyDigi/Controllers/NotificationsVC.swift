import Alamofire

class NotificationsTableViewCell: UITableViewCell{
    @IBOutlet weak var label_Date: UILabel!
    @IBOutlet weak var label_Message: UILabel!
}

class NotificationsVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tableView_Notifications: UITableView!
    let array_Notifications : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView_Notifications.rowHeight = UITableView.automaticDimension
        tableView_Notifications.estimatedRowHeight = 100
        tableView_Notifications.tableFooterView = UIView()
        self.addNavigationControllerActivities()
        //Get All Notifications API Called...
        // self.getAllNotifications()
        let dic1 : NSDictionary = ["time" : "Monday 04:30", "message" : "Reminder! Study for your driving licence!"]
        array_Notifications.add(dic1)
        let dic2 : NSDictionary = ["time" : "Wednesday 14:30", "message" : "Reminder! Study for your driving licence!"]
        array_Notifications.add(dic2)
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //       self.navigationController?.navigationBar.titleTextAttributes =
        //       [NSAttributedString.Key.foregroundColor: UIColor.black,
        //       NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        self.title = "Notifications"
    }
    
    //MARK: API Calling...
    func getAllNotifications(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/get_notification", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            let ary_Day : NSArray! = jsonDict?["day"] as? NSArray
                            let ary_Time : NSArray! = jsonDict!["time"] as? NSArray
                            if ary_Day.count == 0 || ary_Time.count == 0{
                                Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                            }
                            else{
                                for index in 0..<ary_Day.count{
                                    self.array_Notifications.add(String(format: "%@ %@",ary_Day[index] as! NSString, ary_Time?[index] as! NSString))
                                    self.tableView_Notifications.reloadData()
                                    print(index)
                                }
                            }
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        let time : Dictionary =  array_Notifications[indexPath.row] as! Dictionary<String,String>
        cell.label_Date.text =  time["time"] as NSString? as String?
        let message : Dictionary =  array_Notifications[indexPath.row] as! Dictionary<String,String>
        cell.label_Message.text = message["message"] as NSString? as String?
        return cell
    }
    
    //MARK: UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
}
