import Alamofire

class ForgotPasswordVC: UIViewController {
    @IBOutlet weak var textField_UserName: UITextField!
    @IBOutlet weak var button_login: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Code for Email left padding
        textField_UserName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_UserName.frame.height))
        textField_UserName.leftViewMode = .always
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            textField_UserName.layer.cornerRadius = 24
            button_login.layer.cornerRadius = 24
        }
        else{
            textField_UserName.layer.cornerRadius = 20
            button_login.layer.cornerRadius = 20
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_submitClicked(_ sender: UIButton) {
        if textField_UserName.text == ""{
            Utility.showAlert(message:"Ange användarnamn. / Please enter username.", controller: self)
            return
        }
        
        let parameters : Parameters = [
            "username" : textField_UserName.text!
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/retrieve_password", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    @IBAction func button_signUPClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "signUPVC") as? SignUPVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func button_signINClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
