//
//  CheckListViewController.swift
//  esyDigi
//
//  Created by Unyscape Infocom on 10/10/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit
import Alamofire

struct checkListStruct {
    var ID : String?
    var check_status : String?
    var post_title : String?
}

class CheckListTableViewCell: UITableViewCell {
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
}

class CheckListViewController: UIViewController {
    @IBOutlet weak var checkListTableView: UITableView!
    var checkList : [checkListStruct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabels()
        checkListTableView.tableFooterView = UIView()
        self.getCheckListAPICalled()
    }
    
    func setLanguageLabels(){
        self.title = LocalizeHelper.localizedString(forKey: "checklist")
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: API Calling...
    func getCheckListAPICalled(){
        AppDelegate.showHUD(inView: self.view, message: "")
        var projectIDStr = ""
        
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    projectIDStr = Constants.english
                }
                else{
                    projectIDStr = Constants.swedish
                }
            }
        }
        
        let parameters : Parameters = [
            "projectId" : projectIDStr,
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        Alamofire.request(Constants.apiURL+"user/getTopicCheckStaus", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    self.checkList.removeAll()
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            let arr = jsonDict["data"] as! NSArray
                            for i in 0..<arr.count {
                                let dict: NSDictionary = (arr[i] as? NSDictionary)!
                                self.checkList.append(checkListStruct.init(ID: dict["ID"] as? String, check_status: dict["check_status"] as? String, post_title: dict["post_title"] as? String))
                            }
                            self.checkListTableView.reloadData()
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: API Calling...
    func setReadUnreadChapterAPICalled(topicId:String, checkStatus:String){
        AppDelegate.showHUD(inView: self.view, message: "")
        var projectIDStr = ""
        
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "preferredlanguage")
        if (decoded != nil) {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            if (decodedTeams != nil) {
                if decodedTeams as! String == "english"{
                    projectIDStr = Constants.english
                }
                else{
                    projectIDStr = Constants.swedish
                }
            }
        }
        
        let parameters : Parameters = [
            "projectId" : projectIDStr,
            "topicId" : topicId,
            "check" : checkStatus,
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        Alamofire.request(Constants.apiURL+"user/setTopicCheckStaus", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    self.checkList.removeAll()
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            self.getCheckListAPICalled()
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
}

extension CheckListViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.checkList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListTableViewCell", for: indexPath) as! CheckListTableViewCell
        cell.titleLbl.text = self.checkList[indexPath.row].post_title
        if self.checkList[indexPath.row].check_status == "0" {
            cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
        }
        else {
            cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal)
        }
        cell.checkBoxBtn.isUserInteractionEnabled = false
        //        cell.checkBoxBtn?.addTarget(self, action: #selector(checkBoxBtnTap(_:)), for:.touchUpInside)
        //        cell.checkBoxBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let cell = checkListTableView.cellForRow(at: indexPath) as! CheckListTableViewCell
        if(cell.checkBoxBtn.currentImage?.isEqual(#imageLiteral(resourceName: "checkBoxUnselected")))!{
            self.setReadUnreadChapterAPICalled(topicId: self.checkList[indexPath.row].ID!, checkStatus: "1")
        }
        else {
            self.setReadUnreadChapterAPICalled(topicId: self.checkList[indexPath.row].ID!, checkStatus: "0")
        }
    }
    
    //    @objc func checkBoxBtnTap(_ sender: AnyObject) {
    //        let button = sender as? UIButton
    //        let cell = button?.superview?.superview as? CheckListTableViewCell
    //        if(cell?.checkBoxBtn.currentImage?.isEqual(#imageLiteral(resourceName: "checkBoxUnselected")))!{
    //            cell?.checkBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxSelected"), for: .normal)
    //        }
    //        else {
    //            cell?.checkBoxBtn.setImage(#imageLiteral(resourceName: "checkBoxUnselected"), for: .normal)
    //        }
    //    }
}
