import Alamofire
class ChangePasswordVC: UIViewController {
    
    
    @IBOutlet weak var textField_currentPassword: UITextField!
    @IBOutlet weak var textField_NewPassword: UITextField!
    @IBOutlet weak var textField_ConfirmPassword: UITextField!
    @IBOutlet weak var button_Submit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Code for current Password left padding
        textField_currentPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_currentPassword.frame.height))
        textField_currentPassword.leftViewMode = .always
        
        //Code for New Password left padding
        textField_NewPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_NewPassword.frame.height))
        textField_NewPassword.leftViewMode = .always
        
        //Code for Confirm Password left padding
        textField_ConfirmPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_ConfirmPassword.frame.height))
        textField_ConfirmPassword.leftViewMode = .always
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            textField_currentPassword.layer.cornerRadius = 24
            textField_NewPassword.layer.cornerRadius = 24
            textField_ConfirmPassword.layer.cornerRadius = 24
            button_Submit.layer.cornerRadius = 24
        }
        else{
            textField_currentPassword.layer.cornerRadius = 20
            textField_NewPassword.layer.cornerRadius = 20
            textField_ConfirmPassword.layer.cornerRadius = 20
            button_Submit.layer.cornerRadius = 20
        }
        self.addNavigationControllerActivities()
        
        self.textField_currentPassword.placeholder = LocalizeHelper.localizedString(forKey: "Current Password")
        self.textField_NewPassword.placeholder = LocalizeHelper.localizedString(forKey: "New Password")
        self.textField_ConfirmPassword.placeholder = LocalizeHelper.localizedString(forKey: "Confirm Password")
        button_Submit.setTitle(LocalizeHelper.localizedString(forKey: "Submit"), for: .normal)
    }
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //  let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backClicked))
        //   navigationItem.leftBarButtonItem = backButton
        //self.navigationController?.navigationBar.titleTextAttributes =
        //  [NSAttributedString.Key.foregroundColor: UIColor.black,
        //   NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        self.title = LocalizeHelper.localizedString(forKey: "Change Password")
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_submitClicked(_ sender: UIButton) {
        if textField_currentPassword.text == ""{
            Utility.showAlert(message: LocalizeHelper.localizedString(forKey: "Please enter current password.")!, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        if textField_NewPassword.text == ""{
            Utility.showAlert(message:LocalizeHelper.localizedString(forKey: "Please enter new password.")!,  alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        if textField_ConfirmPassword.text == ""{
            Utility.showAlert(message:LocalizeHelper.localizedString(forKey: "Please enter confirm password.")!, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        if textField_NewPassword.text != textField_ConfirmPassword.text{
            Utility.showAlert(message:LocalizeHelper.localizedString(forKey: "New password and confirm password are not same.")!,  alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
            return
        }
        
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "current_password" : textField_currentPassword.text!,
            "new_password" : textField_NewPassword.text!,
            "repeat_new_password" : textField_ConfirmPassword.text!,
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/change_password", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        if (jsonDict["status"] as! String == "ok"){
                            let alert = UIAlertController(title: "Alert", message: "Password change successfully.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    let userDefaults = UserDefaults.standard
                                    userDefaults.removeObject(forKey:"loggedInUserRecord")
                                    userDefaults.synchronize()
                                    self.navigationController?.popToRootViewController(animated: true)
                                case .cancel:
                                    print("cancel")
                                case .destructive:
                                    print("destructive")
                                }}))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
