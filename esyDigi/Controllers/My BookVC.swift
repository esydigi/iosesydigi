import FolioReaderKit
import Alamofire
import IQKeyboardManagerSwift

class My_BookVC: UIViewController,FolioReaderDelegate {
    var string_bookPath = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = false
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        appDelegate.isUserBookOpen = true
        //IQKeyboardManager.shared.disabledToolbarClasses = [My_BookVC.self] //of type UIViewController
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ _animated:Bool) {
        self.openePUBFile(bookPath: string_bookPath as NSString)
    }
    
    func openePUBFile(bookPath:NSString){
        let config = FolioReaderConfig()
        //config.scrollDirection = .vertical
        config.enableTTS = true
        config.displayTitle = true
        config.shouldHideNavigationOnTap = false
        config.allowSharing = false
        config.tintColor = UIColor.init(red: 230/255, green: 110/255, blue: 175/255, alpha: 1.0)
        
        //let bookPath = Bundle.main.path(forResource: "testepub", ofType: "epub")
        //let bookPath = Bundle.main.path(forResource: "moby", ofType: "epub")
        //let bookPath = Bundle.main.path(forResource: "book", ofType: "epub")
        //let bookPath : NSString = String(contentsOf: url) as NSString
        let folioReader = FolioReader()
        folioReader.delegate = self
        //let folioReader1 = FREpubParser
        //folioReader.currentAudioRate = 1n
        //let parsedBook = FRBook()
        //let parsedBook = epubParser.readEpub(epubPath: bookPath, removeEpub: nil, unzipPath: nil)
        //let audioPlayer = foli(withFolioReader: folioReader, book: parsedBook)
        //folioReader.readerAudioPlayer = audioPlayer
        //folioReader.readerContainer = self
        folioReader.presentReader(parentViewController: self, withEpubPath: bookPath as String, andConfig: config)        // Do any additional setup after loading the view.
        // self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func folioReaderDidClose(_ folioReader: FolioReader){
        self.rotateToPotraitScapeDevice()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Rotate screen only in portrait mode
    func rotateToPotraitScapeDevice(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isUserBookOpen = false
        appDelegate.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    @objc func folioReader(_ folioReader: FolioReader, didFinishedLoading book: FRBook) {
        print("loaded")
        //   self.rotateToLandsScapeDevice()
    }    
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)        //self.rotateToPotraitScapeDevice()
    }
    
    func rotateToLandsScapeDevice(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .landscapeLeft
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
}
