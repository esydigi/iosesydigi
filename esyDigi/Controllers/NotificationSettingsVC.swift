import Alamofire

protocol NotificationSettingsTableViewDelegate {
    func buttonDeleteClicked(sender: UIButton)
}

class NotificationSettingsTableViewCell: UITableViewCell{
    @IBOutlet weak var label_Day: UILabel!
    @IBOutlet weak var button_Delete: UIButton!
    var delegate: NotificationSettingsTableViewDelegate?
    
    @IBAction func button_deleteClicked(_ sender: UIButton) {
        delegate?.buttonDeleteClicked(sender: sender)
    }
}

class NotificationSettingsVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,NotificationSettingsTableViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var textField_Picker: UITextField!
    @IBOutlet weak var button_Add: UIButton!
    @IBOutlet weak var tableView_Settings: UITableView!
    var str_Day = LocalizeHelper.localizedString(forKey: "Monday")
    var str_Time = "00:00"
    let array_Settings : NSMutableArray = NSMutableArray()
    // let array_Picker   = [["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],["00:00", "00:30", "01:00","01:30", "02:00","02:30", "03:00","03:30", "04:00","04:30", "05:00","05:30", "06:00","06:30", "07:00","07:30", "08:00","08:30", "09:00","09:30", "10:00","10:30", "11:00","11:30", "12:00","12:30", "13:00","13:30", "14:00","14:30", "15:00","15:30", "16:00","16:30", "17:00","17:30", "18:00","18:30", "19:00","19:30", "20:00","20:30", "21:00","21:30", "22:00","22:30", "23:00","23:30"]]
    let array_Picker   = [[LocalizeHelper.localizedString(forKey: "Monday"), LocalizeHelper.localizedString(forKey: "Tuesday"),LocalizeHelper.localizedString(forKey: "Wednesday"), LocalizeHelper.localizedString(forKey: "Thursday"), LocalizeHelper.localizedString(forKey: "Friday"), LocalizeHelper.localizedString(forKey: "Saturday"), LocalizeHelper.localizedString(forKey: "Sunday")],["00:00", "00:30", "01:00","01:30", "02:00","02:30", "03:00","03:30", "04:00","04:30", "05:00","05:30", "06:00","06:30", "07:00","07:30", "08:00","08:30", "09:00","09:30", "10:00","10:30", "11:00","11:30", "12:00","12:30", "13:00","13:30", "14:00","14:30", "15:00","15:30", "16:00","16:30", "17:00","17:30", "18:00","18:30", "19:00","19:30", "20:00","20:30", "21:00","21:30", "22:00","22:30", "23:00","23:30"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Code for Email left padding
        textField_Picker.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField_Picker.frame.height))
        textField_Picker.leftViewMode = .always
        textField_Picker.placeholder = LocalizeHelper.localizedString(forKey: "Choose Day & Time")
        
        button_Add.setTitle(LocalizeHelper.localizedString(forKey: "Add"), for: UIControl.State.normal)
        tableView_Settings.tableFooterView = UIView()
        self.addNavigationControllerActivities()
        
        //Get All Notifications API Called...
        self.getAllNotifications()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //let backButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(buttonSaveClicked))
     
        let transparentButton = UIButton()
        transparentButton.frame = CGRect.init(x: 0, y: 0, width: 120, height: 40)
        transparentButton.addTarget(self, action:#selector(backClicked), for:.touchUpInside)
        self.navigationController?.navigationBar.addSubview(transparentButton)
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 20)
        button.addTarget(self, action: #selector(buttonSaveClicked), for: .touchUpInside)
        button.setTitle(LocalizeHelper.localizedString(forKey: "Save"), for: .normal)
        button.layer.backgroundColor = UIColor.init(red: 0/255, green: 100/255, blue: 0/255, alpha: 1.0).cgColor
        button.layer.cornerRadius = 5.0
        
        let buttonItem = UIBarButtonItem(customView: button)
        toolbarItems = [buttonItem]
        navigationItem.rightBarButtonItem = buttonItem
        
        //       self.navigationController?.navigationBar.titleTextAttributes =
        //       [NSAttributedString.Key.foregroundColor: UIColor.black,
        //       NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 22)!]
        self.title = LocalizeHelper.localizedString(forKey: "Reminders")
    }
    
    @objc func backClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonSaveClicked(){
        //        if array_Settings.count == 0{//
        //            Utility.showAlert(message:"Please add day and time for notification.", controller: self)
        //            return
        //        }
        
        let array_Day : NSMutableArray = NSMutableArray()
        let array_Time : NSMutableArray = NSMutableArray()
        
        for item in array_Settings {
            let dayandtime : String = item as! String
            let array : NSArray =  dayandtime.components(separatedBy: " ") as NSArray
            array_Day.add(array[0] as! String)
            array_Time.add(array[1] as! String)
        }
        
        for index in 0 ..< array_Day.count {
            if array_Day[index] as! String == "Monday" || array_Day[index] as! String == "Måndag"{
                array_Day.replaceObject(at: index, with: "1")
            }
            else if array_Day[index] as! String == "Tuesday" || array_Day[index] as! String == "Tisdag"{
                array_Day.replaceObject(at: index, with: "2")
            }
            else if array_Day[index] as! String == "Wednesday" || array_Day[index] as! String == "Onsdag"{
                array_Day.replaceObject(at: index, with: "3")
            }
            else if array_Day[index] as! String == "Thursday" || array_Day[index] as! String == "Torsdag"{
                array_Day.replaceObject(at: index, with: "4")
            }
            else if array_Day[index] as! String == "Friday" || array_Day[index] as! String == "Fredag"{
                array_Day.replaceObject(at: index, with: "5")
            }
            else if array_Day[index] as! String == "Saturday" || array_Day[index] as! String == "Lördag"{
                array_Day.replaceObject(at: index, with: "6")
            }
            else if array_Day[index] as! String == "Sunday" || array_Day[index] as! String == "Söndag"{
                array_Day.replaceObject(at: index, with: "7")
            }
        }
        /*
         var string_Day : NSString = ""
         if let data1 = try?  JSONSerialization.data(withJSONObject: array_Day, options: []){
         string_Day = NSString(data: data1, encoding: String.Encoding.utf8.rawValue) ?? ""
         }
         
         var string_Time : NSString = ""
         if let data2 = try?  JSONSerialization.data(withJSONObject: array_Time, options: []){
         string_Time = NSString(data: data2, encoding: String.Encoding.utf8.rawValue) ?? ""
         }
         */
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)",
            "days" : array_Day,
            "time" : array_Time
        ]
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/create_user_noti", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        let jsonDict : NSDictionary! = json as? NSDictionary
                        if (jsonDict["status"] as! String == "ok"){
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                        else{
                            iToast.show(jsonDict["message"] as? String)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    //MARK: API Calling...
    func getAllNotifications(){
        let parameters : Parameters = [
            "user_id" : "\(loggedInUser.shared.string_userID)"
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "")
        Alamofire.request(Constants.apiURL+"user/get_notification", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let jsonDict : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        if (jsonDict["status"] as! String == "ok"){
                            let ary_Day : NSArray! = jsonDict["day"] as? NSArray
                            let ary_Time : NSArray! = jsonDict["time"] as? NSArray
                            if ary_Day.count == 0 || ary_Time.count == 0{
                                Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                            }
                            else{
                                let array_converted = self.changeDayArrayAccordingToCurrentLanguage(array_DayFromServer:ary_Day)
                                for index in 0..<array_converted.count{
                                    self.array_Settings.add(String(format: "%@ %@",array_converted[index] as! NSString, ary_Time?[index] as! NSString))
                                    self.tableView_Settings.reloadData()
                                    print(index)
                                }
                            }
                        }
                        else{
                            Utility.showAlert(message: jsonDict["message"] as! String, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                        }
                    }
                case .failure(let error):
                    Utility.showAlert(message: error.localizedDescription, alertMessage: LocalizeHelper.localizedString(forKey: "Alert")!, controller: self)
                }
        }
    }
    
    func changeDayArrayAccordingToCurrentLanguage(array_DayFromServer:NSArray) -> NSArray{
        let  array_Day : NSMutableArray = NSMutableArray()
        array_Day.addObjects(from: array_DayFromServer as! [Any])
        
        for index in 0 ..< array_Day.count {
            if let day = (array_Day[index] as? String){
                if (day as NSString).integerValue == 1{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Monday")!)
                }
                else if (day as NSString).integerValue == 2{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Tuesday")!)
                }
                else if (day as NSString).integerValue == 3{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Wednesday")!)
                }
                else if (day as NSString).integerValue == 4{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Thursday")!)
                }
                else if (day as NSString).integerValue == 5{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Friday")!)
                }
                else if (day as NSString).integerValue == 6{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Saturday")!)
                }
                else if (day as NSString).integerValue == 7{
                    array_Day.replaceObject(at: index, with: LocalizeHelper.localizedString(forKey: "Sunday")!)
                }
            }
        }
        return array_Day;
    }
    
    //MARK: UIButton Clicks
    @IBAction func pickerViewAdded(_ sender: UITextField) {
        let datePickerView = UIPickerView()
        sender.inputView = datePickerView
        datePickerView.dataSource = self
        datePickerView.delegate = self
        //datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @IBAction func button_addClicked(_ sender: UIButton) {
        if textField_Picker.text == "" || array_Settings.contains(textField_Picker.text!) {return}
        array_Settings.add(textField_Picker.text!)
        tableView_Settings.reloadData()
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationSettingsTableViewCell", for: indexPath) as! NotificationSettingsTableViewCell
        cell.delegate = self
        cell.label_Day.text = array_Settings[indexPath.row] as? String
        cell.button_Delete.setTitle(LocalizeHelper.localizedString(forKey: "Delete"), for: .normal)
        cell.button_Delete.accessibilityLabel = String(format: "%d", indexPath.row)
        return cell
    }
    
    //MARK: UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    func buttonDeleteClicked(sender: UIButton) {
        array_Settings.removeObject(at: Int(sender.accessibilityLabel!)!)
        tableView_Settings.reloadData()
    }
    
    //MARK: UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        if pickerView.tag == 1{
        //            return array_Days.count
        //        }else{
        //            return array_Time.count
        //        }
        return array_Picker[component].count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        //    if pickerView.tag == 1{
        //        return array_Days[row] as String
        //    }else{
        //        return array_Time[row] as String
        return array_Picker[component][row]
    }
    
    //MARK: UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            str_Day = array_Picker[component][row]
        }
        else{
            str_Time =  array_Picker[component][row]!
        }
        textField_Picker.text = String(format: "%@ %@", str_Day ?? "", str_Time)
    }
}
